import Phaser from 'phaser';
import config from 'visual-config-exposer';

class PreGame extends Phaser.Scene {
  constructor() {
    super('bootGame');
    this.GAME_WIDTH = 740;
    this.GAME_HEIGHT = 960;
  }

  preload() {
    this.load.image('bg', config.preGame.bgImg);
    this.load.image('box', config.preGame.boxImg);
    this.load.image('red', config.preGame.confetti1);
    this.load.image('green', config.preGame.confetti2);
    this.load.image('yellow', config.preGame.confetti3);
    this.load.image('blue', config.preGame.confetti4);

    this.load.html('form', '../assets/form.html');

    this.load.audio('flip', config.preGame.flipSound);
  }

  create() {
    const background = this.add.image(0, 0, 'bg');
    background.setOrigin(0, 0);
    background.setScale(config.preGame.bgImgScaleX, config.preGame.bgImgScaleY);

    const title = this.add.text(
      this.GAME_WIDTH / 3.3,
      this.GAME_HEIGHT / 10,
      config.preGame.title,
      {
        fontFamily: 'Helvetica',
        fontSize: '52px',
        fontStyle: 'bold',
        fill: config.preGame.textColor,
      }
    );

    this.logo = this.add.image(
      this.GAME_WIDTH / 2,
      this.GAME_HEIGHT / 3,
      'box'
    );

    this.logo.setDisplaySize(225, 225);

    const playBtn = this.add.text(
      this.GAME_WIDTH / 2.15,
      this.GAME_HEIGHT / 2,
      'Play',
      {
        fontFamily: 'Helvetica',
        fontSize: '32px',
        fill: config.preGame.textColor,
      }
    );

    const leaderBoardBtn = this.add.text(
      this.GAME_WIDTH / 2.7,
      this.GAME_HEIGHT / 2 + 100,
      'Leaderboard',
      {
        fontFamily: 'Helvetica',
        fontSize: '32px',
        fill: config.preGame.textColor,
      }
    );

    let hoverImage = this.add.image(100, 100, 'box');
    hoverImage.setVisible(false);
    hoverImage.setScale(0.07);

    playBtn.setInteractive();
    leaderBoardBtn.setInteractive();

    playBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = playBtn.x - playBtn.width;
      hoverImage.y = playBtn.y + 20;
    });
    playBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
    });
    playBtn.on('pointerdown', () => {
      this.scene.start('playGame');
    });

    leaderBoardBtn.on('pointerover', () => {
      hoverImage.setVisible(true);
      hoverImage.x = leaderBoardBtn.x - leaderBoardBtn.width / 2.5;
      hoverImage.y = leaderBoardBtn.y + 20;
    });
    leaderBoardBtn.on('pointerout', () => {
      hoverImage.setVisible(false);
    });
    leaderBoardBtn.on('pointerdown', () => {
      this.scene.start('leaderBoard');
    });
  }
}

export default PreGame;
